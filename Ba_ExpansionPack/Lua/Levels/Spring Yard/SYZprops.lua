freeslot('SPR_SYZL', 'MT_SYZLIGHT', 'S_SYZLIGHT')

states[S_SYZLIGHT] = {
	sprite = SPR_SYZL,
	frame = FF_PAPERSPRITE|FF_ANIMATE,
	var1 = 3,
	var2 = 5
}

mobjinfo[MT_SYZLIGHT]	= {
	//$Name "Blinking Light"
	//$Sprite SYZLALAR
	//$Category "Spring Yard Zone"
	doomednum = 3192,
	spawnstate = S_SYZLIGHT,
	flags = MF_SCENERY|MF_NOGRAVITY
}